
## Cloning Project

git clone https://gitlab.com/alireza3345/mine.avidrayan-roles-page.git

## Project Directory

cd mine.avidrayan-roles-page

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```


